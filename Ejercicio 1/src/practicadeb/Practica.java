package practicadeb;

import java.util.Scanner;

public class Practica {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las
		 * variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
//El error se produce en el "for" porque en la condici�n 
//" i <= cadenaLeida.length()" el "=" sobra, ya que recorre toda la cadena
//y una posici�n m�s con lo cu�l el programa peta
		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		//for (int i = 0; i < cadenaLeida.length(); i++) {
			//caracter = cadenaLeida.charAt(i);
			//if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
		//		cantidadVocales++;
			//}
			//if (caracter >= '0' && caracter <= '9') {
			//	cantidadCifras++;
			//}
		//}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
