package practicadeb;
import java.util.Scanner;

public class Practica2 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
//El error se produce en el "for" porque en la condici�n 
//" i >= 0" el "=" sobra, ya que recorre todos los numeros
//y el �ltimo que es el 0 no lo puede contar, ya que un n�mero/0 
//es igual a infinito y peta el programa
		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
//for(int i = numeroLeido; i > 0 ; i--){
//		resultadoDivision = numeroLeido / i;
	//	System.out.println("el resultado de la division es: " + resultadoDivision);
	//}		
		lector.close();
	}

}
